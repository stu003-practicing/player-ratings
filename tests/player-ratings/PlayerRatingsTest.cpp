// tests/player-ratings/PlayerRatingsTest.cpp

#include "gtest/gtest.h"

#include "PlayerRatings.h"

TEST(Functionality, AddValues)
{
    PlayerRatings playerRatings;
    //
    const auto name = "Player1 Name1";
    playerRatings.registerPlayerResult(name, 100);
    const auto rank1 = playerRatings.getPlayerRank(name);
    playerRatings.unregisterPlayer(name);
    playerRatings.rollback(1);
    const auto rank2 = playerRatings.getPlayerRank(name);
    //
    ASSERT_EQ(rank1, rank2);
}

// End of File
