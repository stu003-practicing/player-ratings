// lib/player-ratings/PlayerRatings.h
// Created: 27 August 2022

#include "PlayerRatings.h"

#include "SearchTree.h"

void PlayerRatings::registerPlayerResult(const PlayerName& rcPlayerName, PlayerRating playerRating)
{
    mPlayerToRating[rcPlayerName] = playerRating;
    mRatingToPlayers[playerRating].insert(rcPlayerName);
}

void PlayerRatings::unregisterPlayer(const PlayerName& rcPlayerName)
{
    auto pos = mPlayerToRating.find(rcPlayerName);
    if (pos != mPlayerToRating.end())
    {
        auto rating = pos->second;
        mPlayerToRating.erase(pos);
        mRatingToPlayers[rating].erase(rcPlayerName);
    }
}

PlayerRatings::MaybeRank PlayerRatings::getPlayerRank(PlayerName playerName)
{
    auto posPlayer = mPlayerToRating.find(playerName);
    if (posPlayer != mPlayerToRating.end())
    {
        auto posRating = mRatingToPlayers.find(posPlayer->second);
        auto rank = std::distance(posRating, mRatingToPlayers.end());
        //
        return MaybeRank { static_cast <MaybeRank::value_type> (rank) };
    }
    //
    return MaybeRank { };
}

void PlayerRatings::rollback(int step)
{
    //
}

// End of File
