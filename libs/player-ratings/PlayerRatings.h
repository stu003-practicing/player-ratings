// lib/player-ratings/PlayerRatings.h
// Created: 27 August 2022

#ifndef PLAYER_RATINGS_H
#define PLAYER_RATINGS_H

#include <string>
#include <optional>
#include <unordered_set>
#include <unordered_map>

/**
 * Keeps scores of players.
 *
 * Структура данных должна быть “отзывчивой“, т.е. время исполнения каждого запроса (включая rollback)
 * должно быть минимальным и мало зависеть от количества игроков и количества шагов rollback-а (Например
 * вообще не зависеть или зависеть логарифмически). Структура данных должна занимать минимальное достаточное
 * для реализации “отзывчивости” количество памяти.
 */
class PlayerRatings
{
public:
    //
    using PlayerName = std::string;
    using PlayerRating = int;
    using MaybeRank = std::optional <int>;
    //
    /**
     * обновить рейтинг игрока или добавить игрока с рейтингом, если таковой еще не был добавлен
     *
     * @param rcPlayerName
     * @param playerRating
     */
    void registerPlayerResult(const PlayerName& rcPlayerName, PlayerRating playerRating);
    /**
     * забыть игрока
     *
     * @param rcPlayerName
     */
    void unregisterPlayer(const PlayerName& rcPlayerName);
    /**
     * узнать, какое место занимает игрок в рейтинговой таблице (игрок с самым большим рейтингом
     * занимает 1 место)
     *
     * @param playerName The name of the a player to get their rank.
     * @return The
     */
    MaybeRank getPlayerRank(PlayerName playerName);
    /**
     * Откатить step последних запросов registerPlayerResult и unregisterPlayer
     *
     * @param step The number of operations to roll back.
     * @return
     */
    void rollback(int step);
    //
private:
    //
    using PlayerToRating = std::unordered_map <PlayerName, PlayerRating>;
    using RatingToPlayer = std::unordered_map <PlayerRating, std::unordered_set<PlayerName>>;
    //
    PlayerToRating mPlayerToRating;
    RatingToPlayer mRatingToPlayers;
};

#endif     // PLAYER_RATINGS_H

// End of File
