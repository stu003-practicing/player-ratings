// lib/player-ratings/private/PlayerRatings.h
// Created: 1 September 2022

#ifndef SEARCH_TREE_H
#define SEARCH_TREE_H

#include <cstddef>
#include <cassert>

namespace SearchTree
{
    template
    <
        typename TData,
        typename TOrder = std::less <TData>
    >
    struct TreeNode
    {
        TData mData;
        std::size_t muLeftCount = 0u;
        std::size_t muLeftDepth = 0u;
        std::size_t muRightDepth = 0u;
        TreeNode *mpLeft = nullptr;
        TreeNode *mpRight = nullptr;
        TreeNode *mpParent = nullptr;
        //
        template <typename ... TArgs>
        void addLeft(TArgs && ... args)
        {
            assert(mpLeft == nullptr);

        }
    };
}

#endif    // SEARCH_TREE_H

// End of File
